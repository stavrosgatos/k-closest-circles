#include <cuda.h>
#include <cublas.h>
#include <magma.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

static const float overfit_penalty=0.001;
static const float radius_threshold=0.1;
static const unsigned short k_min=2;
static const unsigned short k_max=5;

unsigned short max_iterations;
unsigned short number_of_events;
unsigned short event_points[2048];
struct timeval start_time, end_time;
struct timeval magma_start_time, magma_end_time;
float err1, err2, error;
float* distance;
float*** data;
float errors[k_max-k_min+1];
unsigned short number_of__changes=1;
float total_time=0.0, magma_time=0.0;

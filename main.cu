/* 
This is a CUDA application that implements
the KCC algorithm (K-Closest-Circles). KCC fits
circles to input points. 
*/

#include "KCC.h"
#include "read_events.cu"
#include "min.cu"
#include "KCC.cu"

int main(int argc, char** argv) { 

	// Start timing 
	gettimeofday(&start_time, NULL);
	
	srand(time(NULL)); 
	float final_circles[k_max][k_max][3]; 	
	float circles[k_max][3];
	
	distance=(float*)malloc(sizeof(float) * k_max);
		
    // Store the values from file to data array
	read_events(argv[1]);
	
	// Initialize magma
	magma_init(); 
	
	for (unsigned short event_number = 0; event_number < number_of_events; event_number++) {	// for each event
		unsigned short id=0;
		for (unsigned short K = k_min; K <= k_max; K++) {
			// Run KCC
			KCC(K, event_number, circles); 
			memcpy(final_circles[id], circles, K*3*sizeof(float)); 
			id++;
		}
		// Calculate the minimum error for each K
		unsigned short position = 0;
		float minimum = min(errors, k_max - k_min + 1, &position);
		
		// Print the results
		printf("\n  Event #%d:\n", event_number + 1);
		printf("  _________________________________________\n");	
		for(unsigned short i = 0; i < position + k_min; i++) {
			printf("  Circle #%d:", i + 1);
			for(unsigned short j = 0; j < 2; j++){
					printf(" %f,", final_circles[position][i][j]);
			}
			printf(" %f\n", final_circles[position][i][2]);
		}
		printf("  Error: %f\n", minimum);
		
	}

	free(data);
	free(distance);
	
	// Finalize Magma	
	magma_finalize(); 
	
	// Finish timing for the application
	gettimeofday(&end_time, NULL);
	
	// Calculate and print execution times
	total_time = ((end_time.tv_sec*1000000+end_time.tv_usec) - (start_time.tv_sec*1000000+start_time.tv_usec)) / 1000000.0;
	magma_time /= 1000000.0;
	
	printf("\n  Execution time for magma_dgels_gpu : %f sec.\n\n", magma_time);	
	printf("  Total execution time: %f sec.\n\n", total_time);
	
	return 0;
	
}

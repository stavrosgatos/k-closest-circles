void fitring(float circles[k_max][3], float pts[][2], unsigned short columns, unsigned short event_number, unsigned short K) {

	/*
	Fits the input data points to the equation
	(x-circles[0])^2+(y-circles[1])^2 ] = r^2.
	*/
	
	unsigned short lwork;
	unsigned short rows = 3;
	magma_int_t info; 
	float *A_d, *b_d, *pWork;
	float lWorkQuery[1];
	float A[rows][columns], b[columns], x[columns];
  
	// Transpose pts array
	for(unsigned short i = 0; i < columns; i++) {
		for(unsigned short j = 0; j < 2; j++) {
			A[j][i] = pts[i][j];
		}
		A[2][i] = 1.0;
		b[i] = -(pow(pts[i][0], 2)+pow(pts[i][1], 2));
	}

	cudaMalloc((void**)&A_d, (columns*rows)*sizeof(float));
	cudaMalloc((void**)&b_d, (columns)*sizeof(float));
	
	// Copy host arrays to device
	cublasSetMatrix(columns, rows, sizeof(float), A, columns, A_d, columns);	
	cublasSetVector(columns, sizeof(float), b, 1, b_d, 1);	
	
	// Start timing for magma_sgels_gpu 
	// Use magma_sgels_gpu to solve the linear system A=x\b
	gettimeofday(&magma_start_time, NULL); 
    
	magma_sgels_gpu(MagmaNoTrans, columns, rows, 1, A_d, columns, b_d, columns, lWorkQuery, -1, &info);
 
	lwork = (int)lWorkQuery[0];
	pWork = (float*)malloc(sizeof(float) * lwork);

	magma_sgels_gpu(MagmaNoTrans, columns, rows, 1, A_d, columns, b_d, columns, pWork, lwork, &info);
	
	magma_sgetmatrix(columns, 1, b_d, columns, x, columns);
	
	// Pause the timing until the next iteration
	gettimeofday(&magma_end_time, NULL); 
	
	// Calculate the current execution time for magma_sgels_gpu
	magma_time += ((magma_end_time.tv_sec*1000000+magma_end_time.tv_usec)-(magma_start_time.tv_sec*1000000+magma_start_time.tv_usec));

	// Calculate the circle co-ordinates
	for (unsigned short i = 0; i < 2; i++) {
		circles[K][i] = -0.5 * x[i];
	}
	// Calculate the circle radius
	circles[K][2] = sqrt((pow(x[0],2) + pow(x[1],2)) / 4.0-x[2]);
	
	cudaFree(A_d); 
	cudaFree(b_d);	
	free(pWork);

}


void fit_circles(unsigned short K, unsigned short event_number, float circles[k_max][3], unsigned short* points) {

	/*
	Function to fit circles to the given set of points.
	*/
	
	float pts[event_points[event_number]][2];
	
	for(unsigned short i = 0; i < K; i++) {
		unsigned short k = 0;
		for(unsigned short j = 0; j < event_points[event_number]; j++) {
			if(points[j] == i + 1) {
				pts[k][0] = data[event_number][j][0];
				pts[k][1] = data[event_number][j][1];
				k++;
			}		
		}
		if(k > 3) {
			fitring(circles, pts, k, event_number, i);	
		}
		else {	
			for(unsigned short j = 0; j < 3; j++) {
				circles[i][j] = 0;
			}
		}
	}
	
}


void circle_distance(unsigned short i, unsigned short event_number,  unsigned short K, float circles[k_max][3]) {
	
	/*
	Calculates distance between a point from 
	all circles.
	*/
	
	for(unsigned short j = 0; j < K; j++) {
		distance[j] = pow(pow(data[event_number][i][0]-circles[j][0], 2)
		+ pow(data[event_number][i][1]-circles[j][1], 2)
		- pow(circles[j][2], 2), 2);
	}	
	
}


void find_points(float circles[k_max][3], unsigned short* prev, unsigned short event_number, unsigned short K, unsigned short* points) {

	/*
	Assign points to their closest circles.
	*/
	
	number_of__changes = 0;
	
	for(unsigned short i = 0; i < event_points[event_number]; i++) {
		circle_distance(i, event_number, K, circles);	
		
		unsigned short position = 0;
		// Find the potition of the minimum element
		min(distance, K, &position);		
		points[i] = position + 1;
		if(points[i] != prev[i]) {
			number_of__changes++;
		}
	}

}


void closest_circles(unsigned short K, unsigned short event_number, unsigned short initialize_circles_first, float circles[k_max][3], unsigned short* prev) {

	/*
	K-means extension for fitting circles to the 
	input data according to the Least Absolute
	Deviations metric.
	*/
  
	unsigned short id;
	unsigned short *points;
	points=(unsigned short*)malloc(sizeof(unsigned short)*event_points[event_number] );
	
	if(initialize_circles_first)	{
		for(unsigned short i = 0; i < K; i++)
			for(unsigned short j = 0; j < 2; j++)
				circles[i][j] = (2 * circles[i][j]) - 1;
		number_of__changes = 1;
	}
	else {
		id = 1;
		for(unsigned short i = 0; i < event_points[event_number]; i++) {
			points[i] = id;
			id++;
			if(id > K)
				id = 1;
		}
		fit_circles(K, event_number, circles, points); 
	}
	
	number_of__changes = 1;
	max_iterations = 100;
	while((number_of__changes >= 1) && (max_iterations >= 1)) {
		max_iterations--;
		find_points(circles, prev, event_number, K, points); 
		fit_circles(K, event_number, circles, points);
	}

}


void circle_fit_error(float circles[k_max][3], unsigned short K,  unsigned short event_number, unsigned short *points) {

	/*
	Measure the error in circle fitting.
	*/
	
	error = 0;
	for(unsigned short i = 0; i < event_points[event_number]; i++)	{
		circle_distance(i, event_number, K, circles);
		unsigned short position = 0;
		float minimum = min(distance, K, &position);
		error += minimum;
	}
	error += (overfit_penalty * K * K);
	
}


void prune_circles(float circles[k_max][3], unsigned short *points, unsigned short K, unsigned short event_number, 
unsigned short *pruned_points, unsigned short pruned_circles[k_max][3]) {
	
	/*
	Prune circles with very small radius and ones with less than 4 points.
	*/
	

	unsigned short locations[event_points[event_number]];
	
	for(unsigned short i = 0; i < K; i++)
	{
		unsigned short k = 0;
		for(unsigned short j = 0; j < event_points[event_number]; j++)
			if(points[j] == i + 1 )	{
				locations[k] = j;
				k++;
			}
		if(circles[i][2] < radius_threshold || k < =3) {
			continue;
		}

		for(unsigned short j = 0; j < k; j++)
			pruned_points[locations[j]] = i;
		
		for(unsigned short j = 0; j < 3; j++)
			pruned_circles[i][j] = circles[i][j];
	}	

}


void KCC(unsigned short K, unsigned short event_number, float circles[k_max][3]) {

	unsigned short points[event_points[event_number]], pruned_points[event_points[event_number]], pruned_circles[K][3];	
	float circles1[K][3], circles2[K][3];

	// Random circle initialization
	for(unsigned short i = 0; i < K; i++)
		for(unsigned short j = 0; j < 3; j++) {
			float r = rand() % 1000000 / 1000000.0;
			circles[i][j] = r;
		}

	memcpy(circles1, circles, sizeof(float) * K *3);
	closest_circles(K, event_number, 1, circles1, points);
	prune_circles(circles1, points, K, event_number, pruned_points, pruned_circles); 
	circle_fit_error(circles1, K, event_number, points);
    err1=error;
	
	memcpy(circles2, circles, sizeof(float) * K * 3);
	closest_circles(K, event_number, 0, circles2, points);
	prune_circles(circles2, points, K, event_number, pruned_points, pruned_circles); 
	circle_fit_error(circles2, K, event_number, points);
	err2 = error;
	
	if(err1 < err2) {
		memcpy(circles, circles1, sizeof(float) * K *3);
		errors[K - k_min] = err1;
	} else {
		memcpy(circles, circles2, sizeof(float) * K * 3);
		errors[K - k_min] = err2;
	}


}


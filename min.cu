float min(float arr[], unsigned short size, unsigned short *position) {	
	
	float minimum = arr[0];
	for(unsigned short i = 0; i < size; i++) {
		if(minimum >= arr[i]) {
			*position = i;
			minimum = arr[i];
		}
	}
	return minimum;
	
}

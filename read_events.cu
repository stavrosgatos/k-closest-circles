void read_events(char *location) {

/*
Reads all events from the input file an stores them 
in a 3D array (number_of_events, event_points, 2).
*/

	FILE *file;
	float a;
	file = fopen(location, "r");
	fscanf(file, "%hu", &number_of_events);
	data=(float***)malloc(number_of_events*sizeof(float**));
	
	for (unsigned short i = 0; i < number_of_events; i++) {
		fscanf(file, "%hu", &event_points[i]);
		data[i] = (float**)malloc(event_points[i] * sizeof(float*));
		
		for (unsigned short j = 0; j < event_points[i]; j++) {
			data[i][j] = (float*)malloc(2 * sizeof(float));
			for (unsigned short k = 0; k < 2; k++) {
				fscanf(file, "%f", &a);
				data[i][j][k] = a;
			}
		}
		
	}
	
	fclose(file);

}